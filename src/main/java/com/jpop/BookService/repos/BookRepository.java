package com.jpop.BookService.repos;

import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;
import com.jpop.BookService.models.Book;

@Repository
public interface BookRepository extends CrudRepository<Book, Long> {

}
