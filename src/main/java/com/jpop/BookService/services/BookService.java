package com.jpop.BookService.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jpop.BookService.models.Book;
import com.jpop.BookService.repos.BookRepository;

@Service
public class BookService {
	
	@Autowired
	private BookRepository bookRepository;

	public List<Book> getAllBooks() {
		List<Book> books = new ArrayList<>();
		bookRepository.findAll().forEach(books::add);
		return books;
	}

	public Book addBook(Book book) {
		return bookRepository.save(book);
	}

	public Book getBook(Long id) {
		Optional<Book> book = bookRepository.findById(id);
		if (!book.isPresent())
			return null;
		return book.get();
	}

	public Book updateBook(Long id, Book book) {
		return bookRepository.save(book);
	}

	public void deleteBook(Long bookId) {
		bookRepository.deleteById(bookId);
	}
}
